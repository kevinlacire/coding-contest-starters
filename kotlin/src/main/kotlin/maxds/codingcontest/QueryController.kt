package maxds.codingcontest

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class QueryController {

    @GetMapping
    fun get(@RequestParam("q") query: String): String {
        println("Query is: $query")
        return query
    }
}
