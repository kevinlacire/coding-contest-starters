package maxds.codingcontest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CodingContestApplication

fun main(args: Array<String>) {
    runApplication<CodingContestApplication>(*args)
}
