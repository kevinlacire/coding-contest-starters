import { serve } from "https://deno.land/std@0.138.0/http/server.ts";

serve((req: Request): Response => new Response("Hello World"));