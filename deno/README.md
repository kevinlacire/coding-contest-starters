Deno starter
============

## Getting started

1. install [Deno](https://deno.land/)
2. launch the server using `deno run --allow-net index.ts`

The app start listening on the default port: `8000`
