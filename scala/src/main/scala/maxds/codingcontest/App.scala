package maxds.codingcontest

import cats.effect._
import org.http4s.HttpRoutes
import org.http4s.blaze.server._
import org.http4s.dsl.io._

object App extends IOApp {

    private val service = HttpRoutes.of[IO] {
        case request@GET -> Root =>
            Ok(s"Query is : $q")
    }.orNotFound

    def run(args: List[String]): IO[ExitCode] =
        BlazeServerBuilder[IO]
            .bindHttp(8080, "localhost")
            .withHttpApp(service)
            .serve
            .compile
            .drain
            .as(ExitCode.Success)
}