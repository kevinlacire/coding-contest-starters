from starter import __version__
from starter.main import app
from fastapi.testclient import TestClient

client = TestClient(app)

def test_version():
    assert __version__ == '0.1.0'

def test_root_endpoint():
    response = client.get("/")
    assert response.status_code == 200
    assert response.text == "Hola!"

def test_returns_query_if_given():
    response = client.get("/?q=foo")
    assert response.status_code == 200
    assert response.text == "foo"