# camel + quarkus

1. you must have java & maven installed on your system
2. launch the dev server on default port: `8080`

```shell script
./mvnw compile quarkus:dev
```

## Related Guides

- Camel Platform HTTP ([guide](https://camel.apache.org/camel-quarkus/latest/reference/extensions/platform-http.html)): Expose HTTP endpoints using the HTTP server available in the current platform
