use actix_web::{get, App, HttpRequest, HttpServer};

#[get("/")]
async fn hello(req : HttpRequest) -> String {
    println!("{}", req.query_string());
    String::from("Hey salut bg")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
    })
        .bind(("127.0.0.1", 8080))?
        .run()
        .await
}