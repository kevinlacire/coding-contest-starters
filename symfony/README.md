# dockerForSymfony

Exemple de container Docker pour symfony 6

# Prérequis

- Installer Docker
- Installer docker-compose

# Lancement des conteneurs

```` 
docker-compose -f docker-compose_dev.yml up
```` 

# préparation du projet symfony

- Modifier le fichier .env pour donner les bonnes valeurs (user/password) de la bdd
- Modifier le fichier .env pour donner les bonnes valeurs pour le mailer

# Création du projet symfony

### On se connecte on container php

````
docker-compose -f docker-compose_dev.yml exec php bash
```` 

### On installe symfony

````
symfony new monNomDeProjet --full
cd monNomDeProjet
cp -Rf . ../
cd ..
rm -Rf monNomDeProjet

uniquement en dev
chmod 777 -Rf var/cache/ var/log var/logs 
````

### On vérifie que le host de la machine est bien configuré

```mondomaine.fr``` doit pointer sur localhost. ```mondomaine.fr``` peut être modifié dans le docker-compose_dev.yml
